let accessToken = null;

const authService = {
  setAccessToken: token => accessToken = token,
  getAccessToken: () => accessToken
};

export default authService;
