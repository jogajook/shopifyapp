import Router from "koa-router";
import dotenv from "dotenv";
import axios from "axios";
import { verifyRequest } from "@shopify/koa-shopify-auth";
import authService from "../../services/authService";
import koaBody from "koa-body";

dotenv.config();

const frontRouter = new Router({ prefix: "/front" });

frontRouter.get("/orders", verifyRequest(), async (ctx) => {
  const accessToken = authService.getAccessToken();
  const response = await axios.get(
    `https://${process.env.SHOP}/admin/api/2021-04/orders.json?status=any`,
    {
      headers: {
        "X-Shopify-Access-Token": accessToken
      },
    }
  );
  const jsonText = JSON.stringify(response.data);
  const responseObject = JSON.parse(jsonText);
  ctx.body = responseObject;
});

frontRouter.put("/orders", verifyRequest(), async (ctx) => {
  const data = ctx.request.body;
  console.log('data :>> ', data);
  const accessToken = authService.getAccessToken();
  const response = await axios.put(
    `https://${process.env.SHOP}/admin/api/2021-04/orders/${data.order.id}.json`,
    JSON.stringify(data),
    {
      headers: {
        "X-Shopify-Access-Token": accessToken,
        "Content-Type": "application/json"
      },
    }
  );
  const jsonText = JSON.stringify(response.data);
  const responseObject = JSON.parse(jsonText);
  ctx.body = responseObject;
});

export default frontRouter;

// curl -X GET https://jjteststore888.myshopify.com/admin/api/2021-04/products.json -H 'Content-Type: application/json' -H 'X-Shopify-Access-Token: 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'
