import Router from "koa-router";
// import axios from "axios";
// import * as shopify from "../shopify";
import dotenv from "dotenv";
// import koaBody from "koa-body";

dotenv.config();

const { SHOPIFY_PRIVATE_API_KEY, HOST } = process.env;
const shopifyRouter = new Router({ prefix: "/shopify" });

shopifyRouter.get("/test-delete-store", async (ctx) => {
  ctx.body = {
    status: "success",
    result: {},
  };
});

export default shopifyRouter;
