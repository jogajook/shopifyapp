# Integrate and run the Shopify custom app

- pull the repo master branch
- open teminal and run 
```sh
npm i
```
- Register on [Shopify partners](https://partners.shopify.com/)
- Login as a partner, go to Apps and create a custom app ($APP)
- It will ask to create a development store. Agree and copy the store name ($SHOP)
- Go to Apps -> $APP and copy API Keys and API secret key ($SHOPIFY_API_KEY, $SHOPIFY_API_SECRET)
- Install [ngrok](https://ngrok.com/download)
- Make sure it's in the window $PATH and accessible from the terminal
- Go to the terminal and run 
```sh
ngrok http 8081
```
- Copy the URL https://somechars.ngrok.io from the output, make sure it's HTTPS ($HOST)
- Open the file .env and assign the variables
```sh
SHOPIFY_API_KEY=$SHOPIFY_API_KEY
SHOPIFY_API_SECRET=$SHOPIFY_API_SECRET
SHOP=$SHOP
SCOPES=read_content,write_content,read_themes,write_themes,read_orders,write_orders,write_order_edits,read_draft_orders,write_draft_orders,read_products,write_products,read_product_listings,write_customers
HOST=$HOST
```
- Open one more terminal window and run (don't close ngrok)
```sh
npm run dev
```
- Install the app on the developer store $SHOP if required
- Go to https://$SHOP.shopify.com/ -> Apps -> $APP

