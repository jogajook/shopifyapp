import { createContext } from 'react';

const GlobalContext = createContext({
  authenticatedFetch: null,
  host: null,
  apiHost: null,
  shop: null
});

export default GlobalContext;
