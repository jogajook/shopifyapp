import { Heading, Page } from "@shopify/polaris"
import { parseCookies } from "nookies";

export default function OrderList({ data }) {
  const d = data || [];
  return (
    <Page>
      <Heading>Orders</Heading>
      { data.map(d =>
        <div key={d.id}>{ d.name }</div>
      )}
    </Page>
  )
}

// export async function getStaticPaths() {
//   const paths = getAllPostIds()
//   return {
//     paths,
//     fallback: false
//   }
// }

export async function getServerSideProps(ctx) {

  console.log('FE context :>> ', parseCookies(ctx));

  const data = Promise.resolve([
    {
      id: 1,
      name: 'Order 1 ' + new Date().getTime()
    },  {
      id: 2,
      name: 'Order 2 ' + new Date().getTime()
    },
  ]);

  return {
    props: { data }
  }
}
