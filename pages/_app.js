import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import App from "next/app";
import { AppProvider } from "@shopify/polaris";
import { Provider, useAppBridge } from "@shopify/app-bridge-react";
import { authenticatedFetch } from "@shopify/app-bridge-utils";
import { Redirect } from "@shopify/app-bridge/actions";
import "@shopify/polaris/dist/styles.css";
import translations from "@shopify/polaris/locales/en.json";
import GlobalContext from "../context/GlobalContext";
import dotenv from "dotenv";

dotenv.config();

function userLoggedInFetch(app) {
  const fetchFunction = authenticatedFetch(app);

  return async (uri, options) => {
    const response = await fetchFunction(uri, options);

    if (
      response.headers.get("X-Shopify-API-Request-Failure-Reauthorize") === "1"
    ) {
      const authUrlHeader = response.headers.get(
        "X-Shopify-API-Request-Failure-Reauthorize-Url"
      );

      const redirect = Redirect.create(app);
      redirect.dispatch(Redirect.Action.APP, authUrlHeader || `/auth`);
      return null;
    }

    return response;
  };
}

function MyProvider(props) {
  console.log("props :>> ", props);

  const app = useAppBridge();
  const authenticatedFetch = userLoggedInFetch(app);

  const client = new ApolloClient({
    fetch: authenticatedFetch,
    fetchOptions: {
      credentials: "include",
    },
  });

  const Component = props.Component;
  const config = props.config;

  return (
    <GlobalContext.Provider value={{ authenticatedFetch, ...config }}>
      <ApolloProvider client={client}>
        <Component {...props} />
      </ApolloProvider>
    </GlobalContext.Provider>
  );
}

class MyApp extends App {
  render() {
    const { Component, pageProps, host, shop, apiHost } = this.props;
    return (
      <AppProvider i18n={translations}>
        <Provider
          config={{
            apiKey: API_KEY,
            host: host,
            forceRedirect: true,
          }}
        >
          <div suppressHydrationWarning>
            {typeof window === "undefined" ? null : (
              <MyProvider
                Component={Component}
                config={{ host, shop, apiHost, apiKey: API_KEY }}
                {...pageProps}
              />
            )}
          </div>
        </Provider>
      </AppProvider>
    );
  }
}

MyApp.getInitialProps = async ({ ctx }) => {
  const { host, shop } = ctx.query;
  return {
    host,
    shop,
    apiHost: process.env.HOST,
  };
};

export default MyApp;
