import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import OrderList from "../components/OrderList";
import { Page, Layout } from "@shopify/polaris";

export default function App() {
  const linkStyle = {
    display: "inline",
    marginRight: "20px",
  };
  return (
    <Router>
      <div>
        <ul>
          <li style={linkStyle}>
            <Link to="/">Home</Link>
          </li>
          <li style={linkStyle}>
            <Link to="/orders/list">Orders</Link>
          </li>
        </ul>
        <Switch>
          <Route exact path="/">
            <Page title="Home">
              <Layout>
                <Layout.Section>This is a home  page</Layout.Section>
              </Layout>
            </Page>
          </Route>
          <Route path="/orders/list">
            <OrderList></OrderList>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
