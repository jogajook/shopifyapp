import { useState, useEffect, useContext } from "react";
import GlobalContext from "../context/GlobalContext";
import {
  Card,
  Page,
  DataTable,
  Layout,
  Modal,
  TextContainer,
  TextField,
  Select
} from "@shopify/polaris";
import { useCallback } from "react";

const OrderList = (props) => {
  const globalContext = useContext(GlobalContext);
  const { authenticatedFetch, apiHost } = globalContext;

  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  const getData = async () => {
    return await authenticatedFetch(`${apiHost}/app/front/orders`, {
      method: "GET",
    });
  };

  useEffect(() => {
    const performAction = async () => {
      try {
        const resp = await getData();
        const body = await resp.json();
        setData(body.orders);
        setError(null);
      } catch (error) {
        setError(error);
        setData([]);
      }
    };
    performAction();
  }, []);

  // Modal

  const [active, setActive] = useState(false);

  const handleChange = useCallback(() => {
    setActive(!active);
  }, [active]);

  const handleCancelChange = () => {
    handleChange();
    setActiveOrder(null);
  };

  const handleSaveChange = () => {
    setDataToSave({
      order: {
        id: activeOrder.id,
        email: orderChangeValue,
      }
    });
    handleChange();
    setActiveOrder(null);
  };

  const [activeOrder, setActiveOrder] = useState(null);
  const clickableRowAction = (item) => {
    setActiveOrder(item);
    setOrderChangeValue(item?.email);
    setOrderStatusChangeValue(item.metafields?.find(mf => mf === 'customStatus')?.value || 'CustomDraft');
    handleChange();
  };

  const [orderChangeValue, setOrderChangeValue] = useState("");
  const handleOrderChange = useCallback((newValue) => {
    setOrderChangeValue(newValue);
  }, []);

  const [orderStatusChangeValue, setOrderStatusChangeValue] = useState();
  const handleOrderStatusChange = useCallback((newValue) => {
    setOrderStatusChangeValue(newValue);
  }, []);


  const saveData = async (data) => {
    return await authenticatedFetch(`${apiHost}/app/front/orders`, {
      method: "PUT",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    });
  };

  const [dataToSave, setDataToSave] = useState(null);

  useEffect(() => {
    if (!dataToSave) {
      return;
    }
    const performAction = async () => {
      try {
        const resp = await saveData(dataToSave);
        const body = await resp.json();
        setData(prevData => {
          return prevData.map(item => {
            if (item.id === body.order.id) {
              return body.order;
            }
            return item;
          });
        })
        console.log('body :>> ', body);
        setError(null);
      } catch (error) {
        setError(error);
      } finally {
        setDataToSave(null);
      }
    };
    performAction();
  }, [dataToSave]);

  return (
    <>
      {error && <div>Unable to get data</div>}
      {!error && (
        <Page title="Orders">
          <Layout>
            <Layout.Section>
              <Card>
                <DataTable
                  columnContentTypes={[
                    "text",
                    "text",
                    "text",
                    "text",
                    "numeric",
                    "text",
                    "numeric",
                    "text"
                  ]}
                  headings={[
                    "Order",
                    "Custom Status",
                    "Date",
                    "Customer",
                    "Total",
                    "Payment",
                    "Items",
                    "Email"
                  ]}
                  rows={data.map((item) => [
                    <div onClick={(_) => clickableRowAction(item)}>
                      {item.name}
                    </div>,
                    item.metafields?.find(mf => mf === 'customStatus')?.value || 'CustomDraft',
                    item.created_at,
                    item.customer.name,
                    item.current_total_price,
                    item.financial_status,
                    item.line_items.length,
                    item.email
                  ])}
                />
              </Card>

              <div style={{ height: "500px" }}>
                <Modal
                  open={active}
                  onClose={handleChange}
                  title="Reach more shoppers with Instagram product tags"
                  primaryAction={{
                    content: "Save",
                    onAction: handleSaveChange,
                  }}
                  secondaryActions={[
                    {
                      content: "Cancel",
                      onAction: handleCancelChange,
                    },
                  ]}
                >
                  <Modal.Section>
                    <TextContainer>
                      {activeOrder?.name}

                      <TextField
                        label="New Email"
                        value={orderChangeValue}
                        onChange={handleOrderChange}
                      />

                      <br />

                      {/* <Select
                        label="Custom Status"
                        options={[
                          {label: 'Custom Draft', value: 'CustomDraft'},
                          {label: 'Custom In Progress', value: 'CustomInProgress'},
                        ]}
                        onChange={handleOrderStatusChange}
                        value={orderStatusChangeValue}
                      /> */}
                    </TextContainer>
                  </Modal.Section>
                </Modal>
              </div>
            </Layout.Section>
          </Layout>
        </Page>
      )}
    </>
  );
};

export default OrderList;
